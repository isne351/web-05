<?php

class validation
{

    private $input;

    public function __call($type, $value)
    {
        $this->input = $value[0];

        if (!method_exists($this, $type)) {
            return true;
        }

        return $this->$type();
    }

    private function forname()
    {
        return preg_match('/^[A-z]{3,}$/', $this->input);
    }

    private function surname()
    {
        return preg_match('/^[A-z]{3,}$/', $this->input);
    }

    private function username()
    {
        return preg_match('/^[A-z0-9_-]{5,}$/', $this->input);
    }

    private function password()
    {
        return preg_match('/^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[$@$!%*?&])[A-Za-z\d$@$!%*?&]{8,}/', $this->input);
    }

    private function email()
    {
        return preg_match('/^([A-z0-9]+)@([A-z0-9]+)(\.[A-z0-9]+)$/', $this->input);
    }

    private function yob()
    {
        $now = new DateTime(date('Y-m-d'));

        $yob  = new DateTime(date('Y-m-d', strtotime($this->input)));
        $diff = $now->diff($yob);

        $yeardiff = ((int) $diff->format("%r%y") * -1);

        return (($yeardiff >= 18) && ($yeardiff <= 110));
    }

}

$validation = new validation();

$error_message = array(
    'yob'              => 'It is a 18+ website, so the age must be between 18 and 110',
    'forname'          => 'Forename Must not be blank, must not contain spaces, and must have at least 3 alphabet characters ',
    'surname'          => 'Surname Must not be blank, must not contain spaces, and must have at least 3 alphabet characters ',
    'username'         => 'Username data-error-message="Username Must be at least 8 characters, containing both upper and lower case letters, numbers and symbols',
    'password'         => 'Password Must be at least 8 characters, containing both upper and lower case letters, numbers and symbols',
    'confirm_password' => 'Password not match',
    'email'            => 'Invalid Email!',
);

$error = array();

if (!isset($_POST['password'])) {
    header('locaiton:index.php');
    die();
}

if ($_POST['password'] != $_POST['confirm_password']) {
    $error[] = $error_message['confirm_password'];
}

foreach ($_POST as $key => $value) {
    if ($validation->$key($value) == false) {
        $error[] = $error_message[$key];
    }
}

if(empty($error)){
	echo "All data correct";
}else{
	echo implode("<br>",$error);
}