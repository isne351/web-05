      
      document.getElementById("yob").addEventListener("focus", function(e){
           document.getElementById("yob").setAttribute('type','date');
      });

      document.getElementById("reset").addEventListener("click", function(e){
           document.getElementById("validation_from").reset();
      });

      document.getElementById("validation_from").addEventListener("submit", function(e){
            
      	var element = document.getElementById('validation_from').childNodes;// list all childnode
      
      	var input;
      	var type;
      	var placeholder;
      	var error_message;
      	var success;
      
        for (i = 0; i < element.length; i++) {
      
         type=element[i].id;

         success = true;

         input = document.getElementById(type);
	     	 
	     	 if(input == null) continue;
    	     	 if(input.value == ''){
    	     	 	   input.setAttribute('class','fill-require');
    	     	     continue;
    	     	 }else{
    	     	 	   input.setAttribute('class','');
    	     	 }

             error_message = input.getAttribute("data-error-message");
      
             success = false;

             switch(type){
      
             		case 'forename':
             		case 'surname':
             			
             			var re= /^[A-z]{3,}$/;	
      
      				  	success = re.test(input.value);
      				   
             		break;
             		case 'username':
             			
             			var re= /^[A-z0-9_-]{5,}$/;	
      
      					  success = re.test(input.value);
      
      
             		break;
             		case 'password':
      
             			var re= /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[$@$!%*?&])[A-Za-z\d$@$!%*?&]{8,}/;	
      
      					  success = re.test(input.value);
        
             		break;
             		case 'confirm-password':
      
                  success = false;

             			if(document.getElementById('password').value == document.getElementById('confirm-password').value){
      				   		success = true;
             			}

             		break;
                case 'yob':

                    var diff = new Date(new Date - new Date(input.value)).getFullYear()-1970;

                    if((diff > 18)&&(diff < 110)){
                      success = true;
                    }else{
                       alert(error_message);
                    }

                  break;
             		case 'email':
      
      				    var re = /^([A-z0-9]+)@([A-z0-9]+)(\.[A-z0-9]+)$/;
      
      				    success = re.test(input.value);
      
             		break;
                default:
                  success = true;

                break;
             }
      
             if(success == false){
               	input.value='';
             	  input.setAttribute("placeholder",error_message);
             }
      
          }
        
          console.log(success);

          if(success == false){
              e.preventDefault(); 
          }

      });