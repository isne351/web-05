<html>
   <head>
      <title>Week 5 Form Validation with php</title>
      <meta charset="utf-8">
      <link rel="stylesheet" href="./static/style.css">
   </head>
   <body>
      <form id="validation_from" action="process.php" method="post">
         <h2>SING UP!</h2>
         <input type="text" id="forename" name="forname" placeholder="Forename" data-error-message="Forename Must not be blank, must not contain spaces, and must have at least 3 alphabet characters "> 
         <input type="text" id="surname" name="surname" placeholder="Surename" data-error-message="Surename Must not be blank, must not contain spaces, and must have at least 3 alphabet characters "> 
         <input type="text" id="username" name="username" placeholder="Username" data-error-message="At least 5 characters and can include numbers, _ and –"> 
         <input type="password" id="password" name="password" placeholder="Password" data-error-message="Must be at least 8 characters, containing both upper and lower case letters, numbers and symbols">        
         <input type="password" id="confirm-password" name="confirm_password" placeholder="Confirm password" data-error-message="Password not match"> 
         <input type="date" id="yob" name="yob" placeholder="Date of birth" data-error-message="It is a 18+ website, so the age must be between 18 and 110"> 
         <input type="text" id="email" name="email" placeholder="email@site.com" data-error-message="fill email"> 				
         <input type="submit" value="register" >	

      </form>
   </body>
   <!--script type="text/javascript" src="./static/script.js"></script-->
</html>